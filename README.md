# Nginx LAMP server script for Void Linux
This is a simple script that sets up a LAMP server on a Void Linux system. The packages used are available across all CPU architectures Void Linux an be installed on as well as each libc version.

This script assumes that the server has internet connection already, if this is not the case the base install comes with dhcpcd and wpa_supplicant that can be enabled by symlinking them to /var/service.
