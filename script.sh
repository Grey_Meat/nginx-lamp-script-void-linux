#!/bin/sh

#Install dependences
echo Installing dependences
xbps-install -Suvy
xbps-install nginx mariadb php php-fpm dash -y

#Update nginx configuration
echo Updating nginx configuration
cp -r nginx.conf /etc/nginx/nginx.conf 

#Enable services
echo Enabling services
ln -s /etc/sv/nginx /var/service
ln -s /etc/sv/mysqld /var/service
ln -s /etc/sv/php-fpm /var/service

#Set shell to dash because its faster and POSIX compliant
usermod --shell /bin/dash root
usermod --shell /bin/dash mysql

#Setup mysql
echo Setting up mysql
mariadb-update-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
